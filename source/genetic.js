//Implemetação do Algoritmo Genetico

var AlgoritmoGenetico = function(maximo, melhores){
	this.maximo_unidades = maximo; // Número máximo de unidades na população
	this.melhores_unidades = melhores; // Número de unidades superiores usadas para a evolução da população
	
	if (this.maximo_unidades < this.melhores_unidades) this.melhores_unidades = this.maximo_unidades;
	
	this.Population = []; // Array de todas as unidades na população atual
	
	this.SCALE_FACTOR = 200; // O fator usado para escalar valores de entrada normalizados
}

AlgoritmoGenetico.prototype = {
	// Redefine os parâmetros do algoritmo genético
	reset : function(){
		this.iteration = 1;	// Número de iteração atual (é igual ao número de população atual)
		this.mutateRate = 1; // Taxa de mutação inicial
		
		this.best_population = 0; // O número da população da melhor unidade
		this.best_fitness = 0;  // A aptidão da melhor unidade
		this.best_score = 0;	// A pontuação da melhor unidade de todos os tempos
	},
	
	// Cria uma nova polulação
	CriarPopulacao : function(){
		// Limpa qualquer população existente
		this.Population.splice(0, this.Population.length);
		
		for (var i=0; i<this.maximo_unidades; i++){
			/* Cria uma nova unidade gerando uma rede neural sináptica aleatória com 2 neurônios
			 na camada de entrada, 6 neurônios na camada oculta e 1 neurônio na camada de saída*/
			var newUnit = new synaptic.Architect.Perceptron(2, 6, 1);
			
			// Define parâmetros adicionais para a nova unidade
			newUnit.index = i;
			newUnit.fitness = 0;
			newUnit.score = 0;
			newUnit.isWinner = false;
			
			// Adiciona a nova unidade à população 
			this.Population.push(newUnit);
		}
	},
	
	/* Ativa a rede neural de uma unidade da população para 
	calcular uma ação de saída de acordo com as entradas*/
	ativar : function(bird, target){		
		// Entrada 1: a distância horizontal entre o pássaro e o alvo
		var targetDeltaX = this.normalize(target.x, 700) * this.SCALE_FACTOR;
		
		// Entrada 2: a diferença de altura entre o pássaro e o alvo
		var targetDeltaY = this.normalize(bird.y - target.y, 800) * this.SCALE_FACTOR;
	
		// Cria uma matriz de todas as entradas
		var inputs = [targetDeltaX, targetDeltaY];
		
		// Calcula as saídas ativando a rede neural deste pássaro
		var outputs = this.Population[bird.index].activate(inputs);
			
		// Bate as asas se a saída for superior a 0,5
		if (outputs[0] > 0.5) bird.flap();
	},
	
	// Evolui a população realizando seleção, cruzamento e mutações nas unidades
	evolvePopulation : function(){
		/* Seleciona as unidades superiores da população atual para obter uma 
		matriz de vencedores (eles serão copiados para a próxima população)*/
		var Winners = this.selection();

		if (this.mutateRate == 1 && Winners[0].fitness < 0){ 
			/* Se a melhor unidade da população inicial tiver uma aptidão física negativa
			então significa que não há nenhum pássaro que tenha atingido o primeiro cano!
			Brincando de Deus, podemos destruir essa população ruim e tentar com outra.*/
			this.CriarPopulacao();
		} else {
			this.mutateRate = 0.2; // Caso contrário define a taxa de mutatação para o valor real
		}
			
		// Preencha o resto da próxima população com novas unidades usando crossover e mutação
		for (var i=this.melhores_unidades; i<this.maximo_unidades; i++){
			var paiA, paiB, offspring;
				
			if (i == this.melhores_unidades){
				// O descendente é feito por um crossover de dois melhores vencedores
				paiA = Winners[0].toJSON();
				paiB = Winners[1].toJSON();
				offspring = this.crossOver(paiA, paiB);

			} else if (i < this.maximo_unidades-2){
				// O descendente é feito por um crossover de dois vencedores aleatórios
				paiA = this.getRandomUnit(Winners).toJSON();
				paiB = this.getRandomUnit(Winners).toJSON();
				offspring = this.crossOver(paiA, paiB);
				
			} else {
				// O descendente é um vencedor aleatório
				offspring = this.getRandomUnit(Winners).toJSON();
			}

			// Transforma a prole
			offspring = this.mutation(offspring);
			
			// Cria uma nova unidade usando a rede neural do descendente
			var newUnit = synaptic.Network.fromJSON(offspring);
			newUnit.index = this.Population[i].index;
			newUnit.fitness = 0;
			newUnit.score = 0;
			newUnit.isWinner = false;
			
			// Atualiza a população mudando a unidade antiga com a nova
			this.Population[i] = newUnit;
		}
		
		// Se o melhor vencedor tiver a melhor forma física no histórico, armazene sua conquista!
		if (Winners[0].fitness > this.best_fitness){
			this.best_population = this.iteration;
			this.best_fitness = Winners[0].fitness;
			this.best_score = Winners[0].score;
		}
		
		// Classifica as unidades da nova população em ordem crescente pelo índice
		this.Population.sort(function(unitA, unitB){
			return unitA.index - unitB.index;
		});
	},

	// Seleciona as melhores unidades da população atual
	selection : function(){
		// Classifica as unidades da população atual em ordem decrescente pela sua performance
		var sortedPopulation = this.Population.sort(
			function(unitA, unitB){
				return unitB.fitness - unitA.fitness;
			}
		);
		
		// Marca as unidades superiores como vencedoras!
		for (var i=0; i<this.melhores_unidades; i++) this.Population[i].isWinner = true;
		
		// Retorna uma matriz das unidades superiores da população atual
		return sortedPopulation.slice(0, this.melhores_unidades);
	},
	
	// Executa um cruzamento de ponto único entre dois pais
	crossOver : function(paiA, paiB) {
		// Obtem um ponto de corte do cruzamento
		var cutPoint = this.random(0, paiA.neurons.length-1);
		/* Troca 'bias' informações entre os dois pais:
		1. lado esquerdo para o ponto de cruzamento é copiado de um dos pais
		2. lado direito após o ponto de cruzamento é copiado do segundo pai*/
		for (var i = cutPoint; i < paiA.neurons.length; i++){
			var biasFromParentA = paiA.neurons[i]['bias'];
			paiA.neurons[i]['bias'] = paiB.neurons[i]['bias'];
			paiB.neurons[i]['bias'] = biasFromParentA;
		}

		return this.random(0, 1) == 1 ? paiA : paiB;
	},
	
	// Realiza mutações aleatórias no descendente
	mutation : function (offspring){
		// Transforma algumas informações de 'bias' dos neurônios da prole
		for (var i = 0; i < offspring.neurons.length; i++){
			offspring.neurons[i]['bias'] = this.mutate(offspring.neurons[i]['bias']);
		}
		
		// Transforma algumas informações de 'pesos' das conexões de prole
		for (var i = 0; i < offspring.connections.length; i++){
			offspring.connections[i]['weight'] = this.mutate(offspring.connections[i]['weight']);
		}
		
		return offspring;
	},
	
	// Transforma um gene
	mutate : function (gene){
		if (Math.random() < this.mutateRate) {
			var mutateFactor = 1 + ((Math.random() - 0.5) * 3 + (Math.random() - 0.5));
			gene *= mutateFactor;
		}
		
		return gene;
	},
	
	random : function(min, max){
		return Math.floor(Math.random()*(max-min+1) + min);
	},
	
	getRandomUnit : function(array){
		return array[this.random(0, array.length-1)];
	},
	
	normalize : function(valor, max){
		// Aperta o valor entre os limites min / max
		if (valor < -max) valor = -max;
		else if (valor > max) valor = max;
		
		// Normaliza o valor apertado
		return (valor/max);
	}
}