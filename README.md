﻿Aviso

Se você executar index.html via arquivo: // ou C: /, então o programa não funcionará.
**"Cross origin requests are only supported for HTTP."**
Em vez disso, você precisa executá-lo através de um servidor web.

Exemplo com o NodeJs:
1. npm install -g http-server
2. Na pasta do index.html execute: http-server -c-1
3. Abra o http://localhost:8080
